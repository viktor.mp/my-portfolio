import { Auth, getAuth } from "firebase/auth";
import { initializeApp } from "firebase/app";
import { Database, getDatabase } from "firebase/database";
import { FirebaseStorage, getStorage } from "firebase/storage";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyB7N9GafiPNYu7zcqBAVPsUhRRU1rwCBhU",
    authDomain: "viktor-s-portfolio.firebaseapp.com",
    projectId: "viktor-s-portfolio",
    storageBucket: "viktor-s-portfolio.appspot.com",
    messagingSenderId: "962493412666",
    appId: "1:962493412666:web:f6c95d4ec05449688f0c03",
    measurementId: "G-NCB168FX9B"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth: Auth  = getAuth(app);
export const database: Database = getDatabase(app);
export const storage: FirebaseStorage = getStorage();
export default app;

