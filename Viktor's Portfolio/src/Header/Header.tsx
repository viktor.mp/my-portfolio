import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import { Grid } from '@mui/material';
import FaceIcon from '@mui/icons-material/Face';
import './Header.css'
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';

function ResponsiveAppBar() {

const navigate = useNavigate();

  return (
    <AppBar position="absolute"
    style={{
      display: "flex",
      backgroundColor: "black",
      overflow: "visible",
      top: 0,
      left: 0,
      right: 0,
      margin: 0,
      height: '100px'

    }}>

      <Container maxWidth="xl" sx={{mt: 3}}>
      
        <Grid container sx={{display: 'flex', justifyContent: 'center'}}>
          <Grid item sm={12} md={9.5}>
          <Typography
            variant="h4"
            noWrap
            component={Link} to = '/'
            sx={{
              mt:0.5,
              ml: 2,
              display: { xs: 'flex', md: 'flex' },
              fontFamily: 'Handjet',
              fontWeight: 600,
              letterSpacing: '.2rem',
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            <FaceIcon fontSize='large' sx={{mr: 1}}></FaceIcon>Viktor Petrov / Digital Nomad
          </Typography>
          </Grid>

          <Grid item sm={6} md={1} >
            <Button size='large' 
            onClick={()=> navigate('/resume')}
            sx={{color:'white', fontFamily: 'Lexend Deca', fontSize: '20px'}}>Resume</Button>
            </Grid>
            
            <Grid item sm={6} md={1} >
            
            <Button size='large' 
            onClick={()=> navigate('/projects')}
            sx={{color:'white', fontFamily: 'Lexend Deca', fontSize: '20px'}}>Projects</Button>
            </Grid>

            </Grid>
  
      </Container>
    </AppBar>
  );
}
export default ResponsiveAppBar;