import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import IconButton from '@mui/material/IconButton';
import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import { Avatar, Container } from '@mui/material';
import { Divider } from '@mui/material';

export const ProfileCard = () => {
  return (
    <Card sx={{ backgroundColor: 'grey.200', borderRadius: '0', mt:'10%', maxWidth: '700px' }}>
      <CardContent sx={{p:3, mt:3}}>
        <Avatar
          alt='Viktor-profile-picture'
          src='https://i.ibb.co/3TpgTqf/DSC06810.jpg'
          sx={{ width: 200, height: 200, margin: '0 auto' }}
        />

        <Container sx={{mb:5, mt:5}}>
          <h1>Viktor Petrov</h1>
          </Container>

        <Divider/>
        
        <Container sx={{mb:2, mt:2}}>
        <h3> Front End Developer </h3>
        </Container>

        <Divider/>

        <Container sx={{ textAlign: 'center', mt: 5 }}>
          <IconButton aria-label="Facebook">
            <FacebookIcon />
          </IconButton>
          <IconButton aria-label="LinkedIn">
            <LinkedInIcon />
          </IconButton>
        </Container>
      </CardContent>
    </Card>
  );
};
