import { Container, Grid, Link } from "@mui/material"
import { ImageCarousel } from "../ImageCarousel/ImageCarousel";

const devGemImages = [
    
    'https://gitlab.com/f-ep-group/addonis/-/raw/main/assets/image-4.png',
    'https://gitlab.com/f-ep-group/addonis/-/raw/main/assets/image-7.png',
    'https://gitlab.com/f-ep-group/addonis/-/raw/main/assets/image-8.png',
    'https://gitlab.com/f-ep-group/addonis/-/raw/main/assets/image-10.png',
    'https://gitlab.com/f-ep-group/addonis/-/raw/main/assets/image-13.png',
    'https://gitlab.com/f-ep-group/addonis/-/raw/main/assets/image-14.png',
    'https://gitlab.com/f-ep-group/addonis/-/raw/main/assets/image-16.png',

];

const herStoryImages = [
    'https://i.ibb.co/fQz1xr2/1.png',
    'https://i.ibb.co/LQ75Fcr/2.png',
    'https://i.ibb.co/2d4xcWC/3.png',
]

export const Projects = () => { 
    
    return (
        <>
        <Grid container sx={{display: 'flex', justifyContent: 'center', mt:5,}}>

            <Grid item sm={12} md={12} sx={{mb:8}}>
                <h1>  Projects Portfolio </h1>
            </Grid>

            <Grid item sm={12} md={12} sx={{backgroundColor: 'black', p:3}}>
   
                <h2 style={{textAlign: 'left', color:'white'}}>Dev/Gem - Add-on store</h2>
    
            </Grid>

            <Grid item sm={12} md={12}>

                <Grid container spacing={2}>
                    <Grid item>
                        <Container sx={{borderRight: '1px solid black'}}>
                            <Link
                                href='https://unknown-adonis.web.app'
                                target='_blank'
                                rel='noopener noreferrer'
                                
                                >
                                <h4 style={{textAlign: 'left', color:'black'}}>Visit The Website</h4>
                            </Link>
                        </Container>
                    </Grid>

                    <Grid item>
                        <Container sx={{borderRight: '1px solid black'}}>
                            <Link
                                href='https://gitlab.com/f-ep-group/addonis/-/commits/main'
                                target="_blank"
                                rel="noopener noreferrer"
                                >
                                <h4 style={{textAlign: 'left', color:'black'}}>GitLab Repository</h4>
                            </Link>
                        </Container>
                    </Grid>

                    <Grid item>
                    <Link
                        href='https://gitlab.com/f-ep-group/addonis/-/blob/main/README.md?ref_type=heads'
                        target="_blank"
                        rel="noopener noreferrer"
                        >
                        <h4 style={{textAlign: 'left', color:'black'}}>README Document</h4>
                    </Link>
               
                    </Grid>

                </Grid>
                <hr/>
            </Grid>

                <Container >
                <ImageCarousel images={devGemImages}/>

                </Container>
                
   
        </Grid>

        <Grid container sx={{display: 'flex', justifyContent: 'center', mt:10, backgroundColor: 'grey.200', p:4}}>

            <Grid item sm={12} md={12}>
         
                <h2 style={{textAlign: 'left', color:'black'}}>Project Overview</h2>
                <hr/>
                <p style={{textAlign: 'left', padding:5}}> "DEV/GEM is thoughtfully crafted to provide a user-friendly platform for individuals looking to delve into the world of add-ons. Whether you're a seasoned pro or a newcomer, our platform is tailored to cater to your needs. We've carefully incorporated a wide array of essential features to ensure that every user, whether they've created an account or not, can unlock the full potential of the application."
                </p>
            
            </Grid>

            <Grid item sm={12} md={12}>

         
                <h2 style={{textAlign: 'left'}}>
                    Key Features
                </h2>
    
                <hr/>
            </Grid>

            

            <Grid item sm={12} md={4}>
            <ul>
                <li>User Account</li>
                <li>Upload File (Add-on)</li>
                <li>Search</li>
                <li>Filter and Sort Add-on library</li>
            </ul>

            </Grid>

            
            <Grid item sm={12} md={4}>

            <ul>
                <li>Product Page</li>
                <li>Version Tracking</li>
                <li>Reviews and Ratings</li>
                <li>Notifications</li>
            </ul>
                
            </Grid>

            <Grid item sm={12} md={4}>

            <ul>
                <li>Online Payment and Subscription</li>
                <li>Event Tracking & Custom Analytics</li>
                <li>Add-on Manager</li>
                <li>Admin Panel & more</li>
            </ul>
            </Grid>

           <Grid item sm={12} md={12} sx={{mr:2}}>
            <p>To see a full list of the features, please visit the README file</p>
           </Grid>

            <Grid item sm={12} md={12}>
                
                    <h2 style={{textAlign: 'left'}}>
                        What I learned?
                    </h2>
                    
                    <hr/>
                    
                    <p style={{textAlign:'left'}}>
                    My latest React project was a valuable learning experience. I developed asynchronous backend request functions using Firebase and implemented a custom analytics system to track user events efficiently. Along the way, I faced challenges with JavaScript's number handling, React state management, useEffect, and Context API. This project also marked my introduction to TypeScript, which improved code quality. I leveraged Material UI for the UI components, enhancing the project's aesthetics and usability. In summary, it was a project filled with growth opportunities in areas like async programming, state management, TypeScript, React's core concepts, and the integration of Material UI.
                    </p>
                    <h2 style={{textAlign: 'left'}}>
                       Project Contributors
                    </h2>
                    <hr/>
                    <Grid container>
                        
                        <Grid item sm={4} md={4}>
                            <Link
                            href='https://gitlab.com/viktor.mp'
                            target='_blank'
                            rel='noopener noreferrer'>
                            <h4>Viktor Petrov</h4>
                            </Link>
                           
                        </Grid>

                        <Grid item sm={4} md={4}>
                            <Link
                            href='https://gitlab.com/maria_karamfilova'
                            target='_blank'
                            rel='noopener noreferrer'>
                            <h4> Maria Karamfilova</h4>
                            </Link>
                          
                        </Grid>

                        <Grid item sm={4} md={4}>
                        <Link
                            href='https://gitlab.com/hristiyan.fachikov'
                            target='_blank'
                            rel='noopener noreferrer'>
                            <h4>Hristiyan Fachikov</h4>
                            </Link>
                            
                        </Grid>

                    </Grid>

             
            </Grid>

            
        </Grid>

        <Grid container sx={{display: 'flex', justifyContent: 'center', mt:5,}}>

            <Grid item sm={12} md={12} sx={{p:3, backgroundColor: 'black'}}>

                <h2 style={{textAlign: 'left', color:'white'}}>HerStory - Forum</h2>
            </Grid>

            <Grid item>    
                <Grid container spacing={2}>
                
                    <Grid item>
                        <Container sx={{borderRight: '1px solid black'}}>
                            <Link
                                href='https://gitlab.com/f-ep-group/forum-react'
                                target="_blank"
                                rel="noopener noreferrer"
                                >
                                <h4 style={{textAlign: 'left', color:'black'}}>GitLab Repository</h4>
                            </Link>
                        </Container>
                    </Grid>

                    <Grid item>
                    <Link
                        href='https://gitlab.com/f-ep-group/forum-react/-/blob/main/README.md?ref_type=heads'
                        target="_blank"
                        rel="noopener noreferrer"
                        >
                        <h4 style={{textAlign: 'left', color:'black'}}>README Document</h4>
                    </Link>
               
                    </Grid>

                </Grid>
                <hr/>
            </Grid>

                <Container >
                <ImageCarousel images={herStoryImages}/>

                </Container>
                
   
        </Grid>

        <Grid container sx={{display: 'flex', justifyContent: 'center', mt:10, mb:8, p:4, backgroundColor: 'grey.200' }}>

            <Grid item sm={12} md={12}>
         
                <h2 style={{textAlign: 'left', color:'black'}}>Project Overview</h2>
                <hr/>
                <p style={{textAlign: 'left', padding:5}}> 
                In my first React project, I built a user-friendly web forum. Anyone can use it without signing in, and they can see popular and recent posts. When you sign in, you can do more like creating, editing, and commenting on posts, and even customize your profile. Admins have extra tools to manage users and posts. We also added a simple tagging system to help users find posts they're interested in.
                </p>
            
            </Grid>

            <Grid item sm={12} md={12}>

              
                <h2 style={{textAlign: 'left'}}>
                    Key Features
                </h2>
 
                <hr/>
            </Grid>

            

            <Grid item sm={12} md={4}>
            <ul>
                <li>Create posts with titles and content</li>
                <li>Homepage with key features, user count, and recent posts</li>
                <li>Easy registration and login</li>
                <li>View top 10 most commented posts</li>
            </ul>

            </Grid>

            
            <Grid item sm={12} md={4}>

            <ul>
                <li>See the 10 most recently created posts</li>
                <li>Accessible with authentication</li>
                <li>Simple login and logout for users</li>
                <li>Browse and filter posts from others</li>
                <li>Upvote and Downvote posts</li>
            </ul>
                
            </Grid>

            <Grid item sm={12} md={4}>

            <ul>
                <li>View detailed post content, comments, and likes</li>
                <li>Update user profiles and add profile pictures</li>
                <li>Edit your own posts and comments</li>
            </ul>
                
            </Grid>

            <Grid item sm={12} md={12}>
                
                    <h2 style={{textAlign: 'left'}}>
                        What I learned?
                    </h2>
                    
                    <hr/>
                    
                    <p style={{textAlign:'left'}}>
                    In my first-ever React project, which I embarked on just a week after being introduced to React, I learned a tremendous amount. In the span of two weeks, I not only completed the project but also gained invaluable experience with this new technology. Working collaboratively with my team on GitLab, we efficiently divided tasks into manageable pieces, ensuring constant productivity. This project not only taught me React but also demonstrated the power of teamwork and effective task organization, providing me with essential skills for future endeavors.
                    </p>

                    <h2 style={{textAlign: 'left'}}>
                       Project Contributors
                    </h2>
                    <hr/>

                    <Grid container >
                        
                        <Grid item sm={4} md={4}>
                            <Link
                            href='https://gitlab.com/viktor.mp'
                            target='_blank'
                            rel='noopener noreferrer'>
                            <h4>Viktor Petrov</h4>
                            </Link>
                           
                        </Grid>

                        <Grid item sm={4} md={4}>
                            <Link
                            href='https://gitlab.com/maria_karamfilova'
                            target='_blank'
                            rel='noopener noreferrer'>
                            <h4> Maria Karamfilova</h4>
                            </Link>
                          
                        </Grid>

                        <Grid item sm={4} md={4}>
                        <Link
                            href='https://gitlab.com/hristiyan.fachikov'
                            target='_blank'
                            rel='noopener noreferrer'>
                            <h4>Hristiyan Fachikov</h4>
                            </Link>
                            
                        </Grid>

                    </Grid>
            </Grid>
            
            </Grid>
            
        </>
    )
}