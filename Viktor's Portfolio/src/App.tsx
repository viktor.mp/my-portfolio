import './App.css'
import StickyFooter from './Footer/Footer'
import ResponsiveAppBar from './Header/Header'
import {
  BrowserRouter,
} from "react-router-dom";
import { RoutePaths } from './RouterPaths/RouterPaths';



function App() {

  return (
    <>
  <BrowserRouter>
  <ResponsiveAppBar></ResponsiveAppBar>
  <RoutePaths/>
  <StickyFooter/>
  </BrowserRouter>
  </>
  )
}

export default App
