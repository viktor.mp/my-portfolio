import { Routes, Route } from "react-router-dom";
import { Home } from "../Home/Home";
import { Resume } from "../Resume/Resume";
import { Projects } from "../Projects/Projects";

export const RoutePaths: React.FC = () => { 

    return (
        <Routes>
            <Route path='/' element={<Home/>}/>
            <Route path='/resume' element={<Resume/>}/>
            <Route path='/projects' element={<Projects/>}/>
        </Routes>
    )
}