import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useEffect, useState } from "react";

type ImageCarouselProps = {
  images: string[]
}
type element = React.ReactNode;

export const  ImageCarousel = ({ images }: ImageCarouselProps) => {

  const [imagesToShow, setImagesToShow] =  useState<element[]>([]);

  useEffect(() => {
    const imagesToShow = images.map((image, index) => (
      <div data-index={index} key={image} >
        <img
          src={image}
          alt={`Image ${index}`}
          style={{ maxWidth: "100%" }}
        />
      </div>
      
    ));
    setImagesToShow(imagesToShow);
  }, [images]);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    respondTo: "window",
    arrows: true,

  };

  return (

      <Slider {...settings} >
        {imagesToShow}
      </Slider>

  );
}
