import { useNavigate } from 'react-router-dom';
import { ProfileCard } from '../ProfileCard/ProfileCard'
import { Button, Container, Grid } from '@mui/material'

export const Home = () => {

    const navigate = useNavigate();

    return (
    <Container>
      <Grid container sx={{mb:10}}>
        <Grid item md={6} sm={12} sx={{ display: 'flex', justifyContent: 'flex-end', height: 'auto' }}>
          <ProfileCard />
        </Grid>

        <Grid item md={6} sm={12}>
            <Container sx={{ display: 'flex', justifyContent: 'flex-start', maxWidth: '700px', height: 'auto', mt:'15%', mb:'10%' }}>
        <Container>
        <h1>Hello,</h1>
        <h4> My name is Viktor and I am a netizen who recently got into JS development.</h4>

        <Container sx={{mt: 2, mb: 2}}>
        <Button 
        onClick={()=> navigate('/resume')}
        color= 'secondary' variant='outlined' size='large'>Resume</Button> 
        
        <Button 
        onClick={()=> navigate('/projects')}
        color='secondary' variant='contained' size='large' sx={{m:1}}>Projects</Button>

        <Button color= 'secondary' 
        variant='outlined' 
        size='large'
        href='https://app.enhancv.com/share/9294f23f/?utm_medium=growth&utm_campaign=share-resume&utm_source=dynamic'> My CV </Button>
        </Container>

        {/* <Button variant = 'contained' color ='info' size='large'>Schedule A Meeting</Button> */}

        <p style={{textAlign: 'left'}}>
I'm currently focused on learning web technologies and starting a career in JavaScript development, while also keeping digital marketing and e-commerce as side projects. With a degree from Edinburgh Napier, I transitioned from business management to web development, driven by a passion for web design, user experience, and dynamic web applications. This reflects my commitment to growth and innovation in the field.</p>
        </Container>
            
          </Container>
        </Grid>


      </Grid>
    </Container>
    )
}