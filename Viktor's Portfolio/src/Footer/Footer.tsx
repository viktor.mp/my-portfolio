import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import { Grid, Link } from '@mui/material';

export default function StickyFooter() {
  return (

    <Box
    sx={{
      display: 'flex',
      flexDirection: 'column',
      position: 'fixed', // Use 'fixed' instead of 'absolute'
      bottom: 0, // Fix it to the bottom
      width: '100%',
      left: 0,
      right: 0,
      margin: '4em 0 0 0',
      
    }}
  >
        <CssBaseline />
        
        <Box
          component="footer"
          sx={{
            py: 3,
            px: 2,
            padding: "5px",
            backgroundColor: 'black',
            mt: '20px',
          }}
        >
          <Grid container> 

          <Grid item sm={12} md={4}>
            <h5 className='white-font'>Phone:</h5>
            <h2 className='footer-contact'>+359 88 33 124 27</h2>
          </Grid>

          <Grid item sm={12} md={4}>
            <h5 className='white-font'>Email:</h5>
            <h2 className='footer-contact'>viktor.martinov.p@gmail.com</h2>
          </Grid>

          <Grid item sm={12} md={4}>
            <h5 className='white-font'>LinkedIn:</h5>
            <Link
             href="https://www.linkedin.com/in/viktor-petrov-91a7ab195/"
             target="_blank"
             rel="noopener noreferrer"
            >
            <h2 className='footer-contact'>Click here</h2>
            </Link> 


          </Grid>

          </Grid>
        </Box>
      </Box>

  );
}