import { Avatar, Card, CardContent, Container, Grid } from "@mui/material"
import SouthIcon from '@mui/icons-material/South';

export const Resume = () => { 

    return (
        <>
        
        <Grid container sx={{display: 'flex', justifyContent: 'center', mt:5}}>

            <Grid item sm={12} md={12} sx={{mb:5}}>
                <h1>  Resume </h1>
            <hr/>

            </Grid>
        
            <Grid item sm={12} md={12}>

                <Container style={{maxWidth: '80%'}}>
                <h2 style={{textAlign: 'left'}}>Working Experience</h2>
                </Container>
                
            </Grid>

            <Grid item sm={12} md={12} sx={{display: 'flex', justifyContent: 'center', mb:5, mt:2}}>
                
                <Card sx={{p: 3, backgroundColor: 'grey.200', borderRadius: '0'}}>
                    <CardContent>
                        <Grid container alignItems="center" spacing={2}>
                            <Grid item md={4}>
                                
                                <Avatar
                                alt='Viktor-profile-picture'
                                src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAeFBMVEUJCQn///8AAACRkZHV1dUEBARPT0+zs7P8/PyWlpYsLCy9vb3q6urv7+9kZGSDg4OsrKwSEhIlJSVCQkJUVFT29vZ+fn5MTEympqZfX194eHjd3d3R0dHExMQPDw/k5OSKioqenp4cHBw3NzdqampycnIzMzNERESlE+iDAAADnUlEQVR4nO3ZaXeiMBiGYYxS2UEFLW6I2un//4eDMmwSHDpiTefc1zf1bU4eidmqaQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8DXfSkt+r17lb7NtqjqYcDHs1xPzM97/jKN9+D4D09hJdXHemO8SGrmgZB2qt5c/a8iMIa9fSWdXw2ntTf2oxnrYxZ1dqO+jZamMu+qoESzvsntKbtd4Njo2+6tOi1CXs/w4VvyN52z7XOCWFKi16ZUNfsnp3YdX2wLnsnRGfR/abD5wW8jKt56v5Tv0pvomhq+w9/nZhz6YQ1oGxuCKdfCGkYNyPR/TPZi/e/FLabmpiWfEJ+QsjVeOe0psDIsZeb2mtn6nsLa24tYj9IqrfNax/FqV14CtdpvYEqm5vsTE/7nnhlSHHylqm9nWwyk62druPLF1ytD85HY5WOy67nD1HYHYWr8tfpeh9L31+uz/FiJjqW02eSbG+yfpcJN8fG1kOvzSvxtTaSF2ZfXjl8w3rTL1TvX5nQ+NVa3LXi1zuNz2dvXBSebgt1UTzu99cGkyoTTtudE6bsJ2ZLCr1pbqxywrWk46EsoS9JocbYlCsTSlZlMZctB2cVY9xRJnzrm9D7qQk/JAlXslG6/KmjVDINikORKjKiqHiegaTw08+pOILLhO6+exEIrNPJ8ovCYytGuWGVTMgvV6349s0Yqy3k1z2MZXQWLlWehWq7tq2V36no+eZnX+7Sotml49XJYjtvFIq0+MDV1E6YHeT8T2uWdXtmfa5rR8F87Im4Xbifn7xptaFPFQzYTJgxnCRJmqcQ488JvXn+lRQ6as+l3YpNjNg79wsXKgasEnb2vlpGhHU3YqxkwCrhouMSzawdlYTovvXZtM4miqjt2jzJaX3S3K4KaVHG8ZX8DV7UEgpx3jXuc9ydJ7kRDoPbkI59furF/WPqO+9s+j+u/OluO5lsd/bh7Sh7Lpc18BSP7d02L0vPC03hfK2zxe0lh9TtdYie/+H3dfpL7p2evtCKsBaXRzlYtwY0SEKxvuzcRKzkgjhEQhGOknSUBKPtf5vwukpmR4/ooaH+JMOM0vCy2YnS1hFTBcOM0nxP7hyOCq4agyQMRkYyco2R+6ngQxwk4cwZL0fuIlHzfFhswR75L6bQhJ9s9mKv4CDVxNK8Sh86GujXzZCmYkC1rzoBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU8xvdKyxrHXNLZQAAAABJRU5ErkJggg=='
                                sx={{ width: 120, height: 120, margin: '0 auto' }}
                                />

                                <Container sx={{mb:2, mt:2}}>
                                <h5 style={{color: '#9c27b0'}}>2021-Present</h5>
                                </Container>
                                
                                <h5>WebMaster at predcompa.com</h5>
                                
                            </Grid>
                            
                            <Grid item md={8}>
                                <p style={{ textAlign: 'left' }}>
                                "As the founder of ChuckStar EOOD and predcompa.com, I've successfully managed advertising budgets, enhancing website traffic, revenue, and engagement. My expertise in web design and user experience created an attractive online shopping environment. I also excelled as an SEO Content Strategist, achieving page-one rankings for articles and driving organic traffic. Additionally, I ensured meticulous tax and accounting compliance, facilitating seamless accounting processes."
                                </p>
                                
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>

            <Grid item sm={12} md={12} sx={{mb:3}}>
                <h2> <SouthIcon fontSize="large" /> </h2>
            </Grid>

            <Grid item sm={12} md={12} sx={{display: 'flex', justifyContent: 'center', mb:5}}>

                <Card sx={{p: 3, backgroundColor: 'grey.200', borderRadius: '0'}}>
                    <CardContent>
                        <Grid container alignItems="center" spacing={2}>
                            <Grid item md={4}>
                                   <Avatar
                                alt='Viktor-profile-picture'
                                src='https://cdn.phenompeople.com/CareerConnectResources/CONCGLOBAL/social/Twitter-concentrix-1632389368098.png'
                                sx={{ width: 120, height: 120, margin: '0 auto' }}
                                />

                                <Container sx={{mb:2, mt:2}}>
                                <h5 style={{color: '#9c27b0'}}>2020-2023</h5>
                                </Container>
                                
                                <h5>Senior Customer Support Agent</h5>
                                
                            </Grid>
                            
                            <Grid item md={8}>
                                <p style={{ textAlign: 'left' }}>
                                "I effectively managed a high volume of customer inquiries, averaging 50 tickets daily, showcasing strong communication and problem-solving skills. Handling substantial daily refund amounts of 1000-2000 EUR emphasized my attention to detail and precision. I excelled in troubleshooting and collaborating to enhance user satisfaction, skills crucial for web development. Additionally, I proactively identified and streamlined processes to improve efficiency, a valuable asset in optimizing web applications."
                                </p>
                                
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>

            <Grid item sm={12} md={12} sx={{mb:3}}>
                <h2> <SouthIcon fontSize="large"/> </h2>
            </Grid>

            <Grid item sm={12} md={12} sx={{display: 'flex', justifyContent: 'center', mb:5}}>

                <Card sx={{p: 3, backgroundColor: 'grey.200', borderRadius: '0',}}>
                    <CardContent>
                        <Grid container alignItems="center" spacing={2}>
                            <Grid item md={4}>
                                <Avatar
                                alt='Viktor-profile-picture'
                                src='https://i.ibb.co/qjMQLvs/shanghai-logo.jpg'
                                sx={{ width: 120, height: 120, margin: '0 auto' }}
                                />

                                <Container sx={{mb:2, mt:2}}>
                                <h5 style={{color: '#9c27b0'}}>2017-2020</h5>
                                </Container>
                                
                                <h5>Bartender at Shanghai NightClub Edinburgh</h5>
                                
                            </Grid>
                            
                            <Grid item md={8}>
                                <p style={{ textAlign: 'left' }}>
                                "Thriving in a fast-paced nightclub environment, I honed multitasking and efficient task handling, while making split-second decisions during peak hours, fostering quick thinking and problem-solving skills. Collaborating harmoniously within a team of bartenders, waitstaff, and management, I emphasized the importance of teamwork, a vital trait in software development projects. My interactions with diverse customers improved my customer-centric communication, enhancing my ability to understand individual preferences and provide personalized service, skills that are equally valuable in software development."
                                </p>
                                
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>

        </Grid>

        <hr/>

        <Grid container sx={{display: 'flex', justifyContent: 'center', mt:5}}>

            <Grid item sm={12} md={12}>

                <Container style={{maxWidth: '80%'}}>
                <h2 style={{textAlign: 'left'}}>Education</h2>
                </Container>
                
            </Grid>

            <Grid item sm={12} md={12} sx={{display: 'flex', justifyContent: 'center', mb:5, mt:2}}>
                
                <Card sx={{p: 3, backgroundColor: 'grey.200', borderRadius: '0'}}>
                    <CardContent>
                        <Grid container alignItems="center" spacing={2}>
                            <Grid item md={4}>
                                
                                <Avatar
                                alt='Viktor-profile-picture'
                                src='https://i.ibb.co/Hpr7BN3/telerik-academy-logo.jpg'
                                sx={{ width: 120, height: 120, margin: '0 auto' }}
                                />

                                <Container sx={{mb:2, mt:2}}>
                                <h5 style={{color: '#9c27b0'}}>04/2023-09/2023</h5>
                                </Container>
                                
                                <h5>Telerik Academy</h5>
                                
                            </Grid>
                            
                            <Grid item md={8}>
                                <p style={{ textAlign: 'left' }}>
                                "During my participation in the Alpha JavaScript Track at Telerik Academy from April to September 2023, I achieved advanced JavaScript proficiency, completing core modules with strong problem-solving and object-oriented programming skills. I also delved into data structures and algorithms, optimizing solutions and improving code performance. In web development, I created responsive web pages and a single-page app integrating front-end technologies to display GIFs from an API. Additionally, I demonstrated my React project development skills by crafting an interactive UI Forum and a Web Store for add-ons, highlighting React, UI design, and state management. Explore my projects on GitLab."
                                </p>
                                
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>

            <Grid item sm={12} md={12} sx={{mb:3}}>
                <h2> <SouthIcon fontSize="large" /> </h2>
            </Grid>

            <Grid item sm={12} md={12} sx={{display: 'flex', justifyContent: 'center', mb:5}}>

                <Card sx={{p: 3, backgroundColor: 'grey.200', borderRadius: '0'}}>
                    <CardContent>
                        <Grid container alignItems="center" spacing={2}>
                            <Grid item md={4}>
                                   <Avatar
                                alt='Edinburgh Napier University Logo'
                                src='https://applyzones.com/uploads/logos/AZ-Edinburgh-Napier-University-logo.png'
                                sx={{ width: 120, height: 120, margin: '0 auto' }}
                                />

                                <Container sx={{mb:2, mt:2}}>
                                <h5 style={{color: '#9c27b0'}}>2016-2020</h5>
                                </Container>
                                
                                <h5>BA at Edinburgh Napier University</h5>
                                
                            </Grid>
                            
                            <Grid item md={8}>
                                <p style={{ textAlign: 'left' }}>
                                "My Bachelor of Arts in Business Management and Marketing from Edinburgh Napier University (September 2016 - August 2020) equipped me with a holistic understanding of business operations and strategies. Leveraging my marketing background, I prioritize user needs in front-end development, translating insights into intuitive web interfaces. I excel in aligning web projects with business goals for maximum impact. Additionally, I apply market research techniques from my studies to analyze industry trends, ensuring well-informed web development decisions."
                                </p>
                                
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>

            <Grid item sm={12} md={12} sx={{mb:3}}>
                <h2> <SouthIcon fontSize="large"/> </h2>
            </Grid>

            <Grid item sm={12} md={12} sx={{display: 'flex', justifyContent: 'center', mb:10}}>

                <Card sx={{p: 3, backgroundColor: 'grey.200', borderRadius: '0'}}>
                    <CardContent>
                        <Grid container alignItems="center" spacing={2}>
                            <Grid item md={4}>
                                <Avatar
                                alt='High School of Mathematics Varna Logo'
                                src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAhFBMVEX///8OTKGPr8QHR5/O2+wYVKWXs8gCQ51hisA/cLMKSqCdt8uTscbB0N6huszs8fU3a7GyxdXX4ev19/sTUKPm7PIfWaipv9FxlceSsMnv8/fE0+DM2eQvZa6XsdYlXapLebirwd7E0+dSf7t3m8eDos64yuNqkMOHps3f5/KQrdGtwtnk+rMdAAALFElEQVR4nO2b65aquBZGRYgkeCEKIlYZ7mio/f7vd1bCLVx0j+qtnD+ZY3SfowVVTJJ8a0Xs1Uqj0Wg0Go1Go9FoNBqNRqPRaDQajUaj0Wg0Go1Go9FoNBqNRqPRaDQajeb/xj0nE1j08pR0ekaerqLpu6+AM96KG6TZBCu/vzjFotaEjFlk+u4rMla9WeR0Pp/dhi+J513DF6dwPn3Po/f8d3/Yo9bvTvgLtchY5eR7z0/Js7nLypLf/eF3i6z8vEhrl84GTJKZi22w6Nf4rYxj5MY45NkL/xFvF1ndOWY/547aZW76NESP0RsZycvUW7lpVOZ0dAOstCMbron3i4g/FxZnFfecPZ/xeTq87amvJEOKhpeHe3xEC+WHHxFZuUY6MDlXTzOlis+epHn9RQcJlw7vAFZC1rMiP+xuwmdEYL5UQxP8LOZT7PXI14Mfe+ikvhz/mpK0N+hDIhapqoHK6PrUS/vyVMJo/PPnr4CInuv/8yGRit1OVaXInAJ39kC447LSdIzzrRzExHRgeVLPrg+JuCg7ndLapZZJ5ov7PW9KTWtDRtcTFeqrmRla1KYfEvGECId/To1MxYvZA4ufs1I4v7yvX4u49Sr6pAhOTw1VdcqM+eOsURcwERl0NyOR6h5xzmUh+qjI46Qw+4eyRGSz0tK4UxE1nFURqzTigImSkp0/LMJUkcdcL1/y89n6o3YBT0QamV7kjmNKGfJtwAgIt+jrvcJ/F7ndMMkUkXSuuBsZJPOPmtMvRIBWxAoZCUzTdAzAMUHGpxQ/b+j+UST8ufUit3gawBaB/hLzxkHk1td4hkShms6NSIRIYJtCwjAdBwVBDCPjkycR/68iJ1wQIQDIBT+TOLyAGIi5XCTnuiEkfjFQESJ9ONe/I4ppq+ETSqjP4eyEsXGL+c+4Gff3339CShPKWBzHjOaPnyIw+Oh256mYcj8gcuMkMGCGOIglLFQOi8I+nL9qkTRmttRwHMr8IrVE2+BRHr7Xw4owimETDaNtKsA1xpTEWK2LlUiDIvw5ZxjZ9YR3YMIHBPULV4p06SxEPJ/WHiYihmydLfru1PJEJBIIE7tdiAoOvAc/VPbvkchnGvIC2erBpk3jrgxGobrhFCI8qY82A1bUsSxq4mRx/QNW6cOFxjMOyjX6hBVtxyoqZkoYs8fHw1FtHaxF2nQGETeoJ5YZ0LL9y3nmue8SqSIYCyYiEaaH08ynGSHH9hO/XvZuDClQEN+eETaScCJS7waiRB7vGL0HNDpe9RYR744DQgNYCKBhIljdDFYJZQEYNfmiqlAmryHFMLPyyXDUJj6tZ1f0GInkRIrYidK7VAFj/hs0uE8h123TCShhNAkQ8oP99+3hB4Q8EmaYIxc7ZuIq8c/plsx7SJNoTsSKY/HbTBaoI+BWb/hU6+4n1LENBokeIAPz1LIqF6X7PfzVO9rf9mlBjOH1mghM3OB2S2amVWtL5aXCXlPZomEoNPKumOT5xxn/FUhDMRIoL1OrLa0e+iNExF3fi4qY4mAwxcCkvOcjD1hbAxOCW5FuW4PTUM4sk776pOw/kkDbY5TZoD3oRFK8339/f99uxzBQR8VEELuk93BgYiKE1BXlGCRqIrrb1eCUUHGEQz7QIjIjmkzQTqRix73kdttjNZ9sKPvdSyiCOYcz9mnI+gUFk8ttRRobnAbMFAPygbZqNRd7IHI8yn4iT/cN37eUKJPHDPrRiHnXJxPCOj87LwciYq8WI0dE1vx+8/30Ijzcd9y+c0OdOo2R8djL1lJCy4C29ceMY2ssEoCI47+9Pfy7iEV7kf03ZnSct2bAew0QsVKjqy1w50ciUSJmFkMfmFl/EfH89NibbDGiw3pvspjfBiKwa2pNTMZGIgW2xZR79inZB0Qujcgq5IrIMc5w7itBbFMU8m+Za52IMDG7VfJo9jO1CBZlxPlAEXkqkrYi9+QItCZ56obQkXXjQfx7yEVAtzKyZbJ8YrZD8mgVpUgO6ev4ZKkloo6IGx9bYEQ4dEhlZwJ1LYMh27cm4FL3fhmSjQjUSIKVeXfCBNLXiYP3PmZ7LXK5NHtbfD32/BEVuWRMXqbNxCMEISKTQNI0sRFF9SHkoYjcMGPOZ8r6U5G0E4mwInKUs4KzWIZPII5oRWqbthsvSLN7KoYiMLUWXOsDEYupIoXo4CtOGbQkdX87LwIb2ro7HGRajsTUIkuVw6HIyrheepGtUUZhQDBHZrPjGIjsu/1RSkUNh9s/FfnMZ3FPRA69SBkqIseEUgLbYeSTpqoN8rkXgckl28MAGoIunPMggDK0XGgJkcOhFcnIUTF5UFuUO8fGzUwHkeOcSCX7Q8P5ufXpnEOHsmRoDUXcAIYHkB4XjsU2yzHtPOxFjl2tUbbeXLQjho1vfTjn0DOazF+qQRmJrDC/tBwvFxTCBttHSWyNRKSMIlJ/gGUyJZ0TELHJ7BOKT4lsFZE0v/Qck3vJfq7bnzZDn4qsZIfo+Ol3l2lJ7Dt2slz6CpHtthOpmCJyKUqLHNu6PxY5qiLNkPBbt4KEiMGWS9+RyCr/OfQiV78s4H/idsU+F6mHxM6/ByJLpi+IXDebXoQ/YMl0JpRcL5duZoHI5ZmIHJJ2kUgR2GMu2DJORCxyOUiExwGLmdY/wAqLpyJySBx06URy2Kksmb5jEXi5PXRwDEb9dzrCog/nsYgrKrnB920454kN6btYyzgRgemjiIjRufafcDYiTaaNPsItYUjsx77NNEJNO/nlV9L+CRDZ7RSRe3LYblWTsP8E+qVIFQSOnfci0GuSV1/HezdjETfYbLaSRkV55hkWh+ciK5zbJu1WkNggLhlaQmS9Vh8aYt6ISJntVZnnA5HL5GEoRmbQN2rMRp95oP6EiUiEN4LGZBMqNQ1E2kCbFUlsf9uJIHu5j4IEE5GKbTct282BKI8SG5F68cyIcMfoSmZoLLvWpyIr/7rrTDZXpCRoWKiJNhU5Mrto9wGFbZJytSBTkTJcdx47rgZPWGy2L0WwjY9NqclhiSxY1+dEMrLZdWMy+P63EOnybEbkcnXyY7OCYpsuukRmRFx4ZyeAAdkMHgqEoRpoMyIXQpoR4ebg0eECTEVWuKhFdrs1H2woQKQPtAObEQlJU2SIjZbsGFfiS7DrsUiaw1u1CR6UtEakybOpyOGwwTKcj8Wyu0NBFYsR4cO3duuazfBLwGGo5NmsyIVv4F9HjqA/efN/XfE3LLZZrx/D6ZzzRoQPK8HfRURzdjk8fOULEUtxT6YXzHEjMpxZ9eLpRMbpWuI6zQrqIzI+9fPwB1zwoOyJZ1f13LqObjppl44sMeP/MieUTdoB+oIrjEqx4Ha9vjg5jYYJ46FmZg2/bOHG1zbORKKFw0v1/KvSNm+ub/iixm+4E3nJ4XBGh4V89zF83CTTrAu09ehS76Tp0doys2j63v1mXQ/7Irlw1rvhOFntsbXMeper52Q+75NAtM0cL9bE39Mw6K6N5FHaZa0byPf6W26laYmK9ZArnHO/eyvvLn5ToSSapEBFKn78cXyShNf+sjgmfdXAQrBfrxZNEnxdj9nxnBDkoQR+k9KfdYQ5SdDHTdxgcmX9Bz+RCOB+K1KOB6Nnw6x4CxNNybOe7eYyrjYLi1Rsp2ZyWfA5CQmIbJSl0/GkbH5EZMRGKWI+3xR9kpXhbnxwhxgRcfI8C4isIhYMMZQMvueG8p1eNzSCJzC+Kp/9TLDoLlGj0Wg0Go1Go9FoNBqNRqPRaDQajUaj0Wg0Go1Go9FoNBqNRqPRaDQajeYJ/wNsf2GCAW/UnAAAAABJRU5ErkJggg=='
                                sx={{ width: 120, height: 120, margin: '0 auto' }}
                                />

                                <Container sx={{mb:2}}>
                                <h5 style={{color: '#9c27b0'}}>2011-2026</h5>
                                </Container>
                                
                                <h5>High School of Mathematics Varna</h5>
                                
                            </Grid>
                            
                            <Grid item md={8}>
                                <p style={{ textAlign: 'left' }}>
                                "My Diploma in English and Information Technology from the High School of Mathematics in Varna, Bulgaria, was instrumental in my journey. It empowered me with English proficiency, crucial for global collaboration in the programming community. I gained hands-on experience in IT classes, covering fundamental concepts like HTML, CSS, PhotoShop, and binary numbers, forming the basis of my web development skills. Immersed in mathematics classes, I honed problem-solving and logical thinking abilities, essential for crafting efficient and well-structured code."
                                </p>
                                
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>
            
        </Grid>

        </>
    )
}